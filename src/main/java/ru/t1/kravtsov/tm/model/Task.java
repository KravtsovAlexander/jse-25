package ru.t1.kravtsov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.model.IWBS;
import ru.t1.kravtsov.tm.enumerated.Status;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Task extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @Nullable
    private String projectId;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @NotNull
    private Date created = new Date();

    public Task(final @NotNull String name) {
        this.name = name;
    }

    public Task(final @NotNull String name, final @NotNull String description) {
        this.name = name;
        this.description = description;
    }

}
