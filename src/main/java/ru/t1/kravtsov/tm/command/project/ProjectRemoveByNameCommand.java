package ru.t1.kravtsov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.model.Project;
import ru.t1.kravtsov.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Remove project by name.";

    @NotNull
    public static final String NAME = "project-remove-by-name";

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY NAME]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final List<Project> projectsForDeletion = new ArrayList<>();
        for (final Project project : getProjectService().findAll(getUserId())) {
            if (project.getName().equals(name)) {
                projectsForDeletion.add(project);
            }
        }
        getProjectTaskService().removeProjects(getUserId(), projectsForDeletion);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
