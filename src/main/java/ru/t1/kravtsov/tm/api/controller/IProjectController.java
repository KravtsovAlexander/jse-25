package ru.t1.kravtsov.tm.api.controller;

public interface IProjectController {

    void displayProjects();

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectsByName();

    void displayProjectById();

    void displayProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

    void startProjectById();

    void startProjectByIndex();

    void completeProjectById();

    void completeProjectByIndex();

    void changeProjectStatusById();

    void changeProjectStatusByIndex();

}
